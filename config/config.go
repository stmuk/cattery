package config

import (
	"errors"
	"os"
	"strconv"
)

func GetHttpPort() (httpPort string, err error) {
	httpPort = os.Getenv("HTTP_PORT")
	if httpPort == "" {
		err = errors.New("HTTP_PORT undefined")
	}

	return httpPort, err
}

func GetGrpcPort() (grpcPort string, err error) {
	grpcPort = os.Getenv("GRPC_PORT")
	if grpcPort == "" {
		err = errors.New("GRPC_PORT undefined")
	}

	return grpcPort, err
}

func GetGrpcMsgSize() (grpcMsgSize int, err error) {
	grpcMsgSize, err = strconv.Atoi(os.Getenv("GRPC_MSG_SIZE"))
	if err != nil {
		err = errors.New("GRPC_MSG_SIZE must be numeric")
	}

	return grpcMsgSize, err
}

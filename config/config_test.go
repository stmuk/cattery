package config

import (
	"os"
	"testing"
)

func TestGetHttpPort(t *testing.T) {
	os.Setenv("HTTP_PORT", "2112")
	_, err := GetHttpPort()
	if err != nil {
		t.Fail()
	}

	os.Setenv("HTTP_PORT", "")
	_, err = GetHttpPort()
	if err == nil {
		t.Fail()
	}
}

func TestGetGrpcPort(t *testing.T) {
	os.Setenv("GRPC_PORT", "2112")
	_, err := GetGrpcPort()
	if err != nil {
		t.Fail()
	}
	os.Setenv("GRPC_PORT", "")
	_, err = GetGrpcPort()
	if err == nil {
		t.Fail()
	}
}

func TestGetGrpcMsgSize(t *testing.T) {
	os.Setenv("GRPC_MSG_SIZE", "2112")
	_, err := GetGrpcMsgSize()
	if err != nil {
		t.Fail()
	}

	os.Setenv("GRPC_MSG_SIZE", "")
	_, err = GetGrpcMsgSize()
	if err == nil {
		t.Fail()
	}
}

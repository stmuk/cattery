# These dynamically encode version numbers in the binaries & docker tags
VERSION := $(shell git diff --quiet HEAD; git describe --tags --always --long --dirty | sed 's/-/+/' | sed 's/-/./g')
TAG := $(shell git diff --quiet HEAD; git describe --tags --always --dirty | sed 's/-/+/' | sed 's/-/./g')

# pull in the port definitions in the 'env' file as env vars expanded below
include env
export

# build locally for dev you will have to use . ./env to set environment in your
# shell
all:
	go build -ldflags="-X 'main.Version=$(VERSION)'" ./cmd/grpc-server
	go build -ldflags="-X 'main.Version=$(VERSION)'" ./cmd/grpc-upload 

# quick integration test target
.PHONY: int-test
int-test: all
	GRPC_MSG_SIZE=$(GRPC_MSG_SIZE) HTTP_PORT=$(HTTP_PORT) ./grpc-upload ./image/coolcat.jpeg

.PHONY: test
test:
	go test -cover -coverprofile coverage.out ./...

.PHONY: cover
cover: test
	@go tool -cover -html=coverage.out -o coverage.html

# useful and generally quiet linter
.PHONY: staticcheck
staticcheck:
	 go install honnef.co/go/tools/cmd/staticcheck@latest
	 staticcheck ./...

# regenerate cattery.pb.go & cattery_grpc.pb.go if cattery.pb.go is changed
.PHONY: proto
proto:
	protoc --go_out=.  --go_opt=paths=source_relative --go-grpc_out=. --go_opt=paths=source_relative cattery/cattery.proto

# scan module deps for security issues which alerts about its own deps 20210624! has been useful in the past
nancy/nancy:
	@if [ ! -d nancy ]; then git clone --depth 1 https://github.com/sonatype-nexus-community/nancy.git; fi
	cd nancy && go build

.PHONY: security
security: nancy/nancy
	@if [ -f go.sum ]; then go list -json -m all | ./nancy/nancy sleuth; fi
	@if [ -f Gopkg.lock ]; then ./nancy/nancy sleuth -p Gopkg.lock; fi

# targets to manage docker
.PHONY: docker-build
docker-build: 
	docker build  --build-arg GRPC_PORT=$(GRPC_PORT) --build-arg GRPC_MSG_SIZE=$(GRPC_MSG_SIZE) --build-arg HTTP_PORT=$(HTTP_PORT) -t cattery-latest .

.PHONY: docker-run
docker-run:
	docker run -p  $(GRPC_PORT):$(GRPC_PORT) -p $(HTTP_PORT):$(HTTP_PORT) cattery-latest

.PHONY: docker-release
docker-release: 
	@echo "IMPORTANT: docker release tagging a dirty git checkout won't work"
	docker build  --build-arg GRPC_PORT=$(GRPC_PORT) --build-arg GRPC_MSG_SIZE=$(GRPC_MSG_SIZE) --build-arg HTTP_PORT=$(HTTP_PORT) -t "cattery-$(TAG)" .

package main

import (
	"log"
	"net"
	"net/http"
	"runtime"

	"gitlab.com/stmuk/cattery/cattery"
	"gitlab.com/stmuk/cattery/config"
	"gitlab.com/stmuk/cattery/file"
	"google.golang.org/grpc"
)

// defined at compile time
var (
	Version   = "undefined"
	GoVersion = runtime.Version()
)

func main() {
	log.SetFlags(log.LstdFlags | log.Lshortfile)

	grpcPort, err := config.GetGrpcPort()
	if err != nil {
		log.Fatal(err)
	}

	httpPort, err := config.GetHttpPort()
	if err != nil {
		log.Fatal(err)
	}

	size, err := config.GetGrpcMsgSize()
	if err != nil {
		log.Fatal(err)
	}

	log.Printf("starting 'grpc-server' version: '%s' on gRPC port %s, HTTP port %s with msg size %d", Version, grpcPort, httpPort, size)

	l, err := net.Listen("tcp", ":"+grpcPort)
	if err != nil {
		log.Fatalf("grpc failed to listen: %v", err)
	}

	s := grpc.NewServer(grpc.MaxRecvMsgSize(size))
	cattery.RegisterFileServer(s, &file.Server{})

	// It's possible to create a mux to mix gRPC and HTTP < 2 handling on a port
	// but didn't seem like a good idea!

	go func() {
		http.Handle("/", http.FileServer(http.Dir(".")))

		if err := http.ListenAndServe(":"+httpPort, nil); err != nil {
			log.Fatalf("http failed to listen: %v", err)
		}
	}()

	if err := s.Serve(l); err != nil {
		log.Fatalf("failed to serve: %v gRPC", err)
	}
}

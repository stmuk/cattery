package main

import (
	"context"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"runtime"
	"time"

	"gitlab.com/stmuk/cattery/cattery"
	"gitlab.com/stmuk/cattery/config"
	"google.golang.org/grpc"
)

// defined at compile time
var (
	Version   = "undefined"
	GoVersion = runtime.Version()
)

// client

func main() {
	log.SetFlags(log.LstdFlags | log.Lshortfile)

	grpcPort, err := config.GetGrpcPort()
	if err != nil {
		log.Fatal(err)
	}

	size, err := config.GetGrpcMsgSize()
	if err != nil {
		log.Fatal(err)
	}

	log.Printf("starting 'grpc-upload' version: '%s' calling gRPC port %s with max msg size %d", Version, grpcPort, size)

	if len(os.Args) == 1 {
		log.Fatal("must pass image name (png, jpg & gif) as single arg")
	}

	fn := os.Args[1]
	b, err := ioutil.ReadFile(fn)

	if err != nil {
		log.Fatal(err)
	}

	if len(b) > size {
		log.Fatalf("not uploading max file size is %d", size)
	}

	conn, err := grpc.Dial(":"+grpcPort, grpc.WithInsecure())
	if err != nil {
		log.Fatalf("could not connect to backend: %v", err)
	}
	defer conn.Close()
	client := cattery.NewFileClient(conn)

	ctx, cancel := context.WithTimeout(context.Background(), time.Second)
	defer cancel()

	resp, err := client.Upload(ctx, &cattery.UploadReq{Filename: fn, Filecontents: b})
	if err != nil {
		log.Print(err)
	}

	if resp.Error != "" {
		fmt.Printf("%s\n", resp.Error)
	} else {
		fmt.Printf("%s\n", resp.Url)
	}
}

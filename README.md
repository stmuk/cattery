# cattery

# Description

A gRPC client and a gRPC/HTTP server (using two ports).

The client is used to upload cat pictures (and other images) to the server
via gRPC

A standard HTTP client like curl can be used to download the images.

# Dependences

* Docker 

* go1.16.5 (earlier versions probably work but are untested)

* GNU Make

* staticcheck linter (https://staticcheck.io/)
  * optional on docker host but can be installed via `make staticcheck`

# Configuration

* Via environment variables set by `env` file

* This is directly read by the Makefile/Dockerfile

* Note if you are developing outside Makefile/Dockerfile you will need to:

```
. ./env
```

to set the environment variables in your shell.

## QUICKSTART

```
make docker-build
```

* Builds and tags `cattery-latest` docker image using files in the local directory.

* If local motifications have been done the version number is marked as
  "dirty".  Tests and staticcheck will have to pass

```
make docker-run
```

* runs  `cattery-latest` docker image and forwards ports 8888 (gRPC) & 3000
  (HTTP) onto localhost of docker host

Open another window window and 

```
make int-test
```

Expected output in last line is `http://localhost:3000/coolcat.jpeg`

Use curl (or similar) to download
```
curl -O http://localhost:3000/coolcat.jpeg
```

Confirm via `xdg-open coolcat.jpeg` (Linux) or `open coolcat.jpeg` (macOS)

There are a number of Makefile targets (with comments) for development use.

## RELEASES

```
git tag v0.99
make docker-release
```

* Creates a docker release image tagged as `cattery-v0.99`

* Needs to be done on a clean (not git "dirty" with local modifications) checkout

* Again staticcheck and tests must pass

## BUGS AND RESTRICTIONS

* No bugs are known but are sure to exist.

* This is a minimal viable product.  There is a size limit on the image. This
  is enforced in the client and configurable via GRPC_MSG_SIZE and set to 
  4194304 by default.

* gRPC Client streaming where the image is broken up into chunks would be a
  better long-term solution for upload.

* The web server lacks access control and authentication.  It will serve any
  file in the directory.

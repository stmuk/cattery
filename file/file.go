package file

import (
	"context"
	"log"
	"net/http"
	"os"
	"path/filepath"

	"gitlab.com/stmuk/cattery/cattery"
)

type Server struct {
	cattery.UnimplementedFileServer
}

func (s *Server) Upload(ctx context.Context, ur *cattery.UploadReq) (*cattery.UploadResp, error) {
	port := os.Getenv("HTTP_PORT")

	log.SetFlags(log.LstdFlags | log.Lshortfile)
	log.Printf("got ur.Filename %#v", ur.Filename)

	var resp *cattery.UploadResp

	fn := filepath.Base(ur.Filename)

	fo, err := os.Create(fn)
	if err != nil {
		log.Print(err)
		return resp, err
	}

	fileType := http.DetectContentType(ur.Filecontents)
	// pass as env var?
	if !ValidateImageType(fileType, []string{"image/jpeg", "image/png", "image/gif"}) {
		return &cattery.UploadResp{Error: "not valid image type"}, nil
	}

	_, err = fo.Write(ur.Filecontents)
	if err != nil {
		log.Print(err)
		return resp, err
	}

	if err := fo.Close(); err != nil {
		log.Print(err)
		return resp, err
	}

	resp = &cattery.UploadResp{Url: "http://localhost:" + port + "/" + fn}

	log.Printf("DEBUG: send resp %#v", resp)

	return resp, nil

}

func ValidateImageType(actualImage string, permittedImages []string) bool {
	ok := false

	for _, i := range permittedImages {
		if actualImage == i {
			ok = true
			break
		}
	}

	return ok
}

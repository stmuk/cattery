package file

import (
	"context"
	"io/ioutil"
	"log"
	"net"
	"os"
	"testing"

	"gitlab.com/stmuk/cattery/cattery"
	"google.golang.org/grpc"
	"google.golang.org/grpc/test/bufconn"
)

const bufSize = 1024 * 1024

var l *bufconn.Listener

func init() {
	l = bufconn.Listen(bufSize)
	s := grpc.NewServer()
	cattery.RegisterFileServer(s, &Server{})
	go func() {
		if err := s.Serve(l); err != nil {
			log.Fatalf("Server exited with error: %v", err)
		}
	}()
}
func bufDialer(context.Context, string) (net.Conn, error) {
	return l.Dial()
}

func TestUpload(t *testing.T) {
	ctx := context.Background()
	conn, err := grpc.DialContext(ctx, "bufnet", grpc.WithContextDialer(bufDialer), grpc.WithInsecure())
	if err != nil {
		t.Fatalf("Failed to dial bufnet: %v", err)
	}
	defer conn.Close()
	client := cattery.NewFileClient(conn)

	fn := "cat.jpg"
	// image/jpeg magic
	fc := []byte("\xff\xd8\xff")
	resp, err := client.Upload(ctx, &cattery.UploadReq{Filename: fn, Filecontents: fc})
	if err != nil {
		t.Fatalf("Upload failed: %v", err)
	}

	got := resp.Error

	if got != "" {
		t.Fatalf("got %s", got)
	}

	got = resp.Url
	if got != "http://localhost:3000/cat.jpg" {
		t.Fatalf("got %s", got)
	}

	b, err := ioutil.ReadFile(fn)
	if err != nil {
		t.Fatalf("Not written to filesys: %v", err)
	}

	got = string(b)
	if got != string(fc) {
		t.Errorf("got %s", got)
	}

	if err := os.Remove(fn); err != nil {
		t.Fatalf("can't remove file: %v", err)
	}

	// text
	fc = []byte("abcde")
	resp, err = client.Upload(ctx, &cattery.UploadReq{Filename: fn, Filecontents: fc})
	if err != nil {
		t.Fatalf("Upload failed: %v", err)
	}

	got = resp.Error
	if got != "not valid image type" {
		t.Fatalf("got %s", got)
	}

	if err := os.Remove(fn); err != nil {
		t.Fatalf("can't remove file: %v", err)
	}

}

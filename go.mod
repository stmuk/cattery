module gitlab.com/stmuk/cattery

go 1.16

require (
	golang.org/x/crypto v0.0.0-20210616213533-5ff15b29337e // indirect
	google.golang.org/grpc v1.38.0
	google.golang.org/protobuf v1.26.0
)
